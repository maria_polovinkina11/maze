﻿namespace GenMaze
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_GenMaze = new System.Windows.Forms.Button();
            this.button_FindExit = new System.Windows.Forms.Button();
            this.button_SaveMaze = new System.Windows.Forms.Button();
            this.pictureMaze = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.txtWidth = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureMaze)).BeginInit();
            this.SuspendLayout();
            // 
            // button_GenMaze
            // 
            this.button_GenMaze.Location = new System.Drawing.Point(12, 27);
            this.button_GenMaze.Name = "button_GenMaze";
            this.button_GenMaze.Size = new System.Drawing.Size(167, 57);
            this.button_GenMaze.TabIndex = 0;
            this.button_GenMaze.Text = "Сгенерировать лабиринт";
            this.button_GenMaze.UseVisualStyleBackColor = true;
            this.button_GenMaze.Click += new System.EventHandler(this.button_GenMaze_Click);
            // 
            // button_FindExit
            // 
            this.button_FindExit.Location = new System.Drawing.Point(220, 27);
            this.button_FindExit.Name = "button_FindExit";
            this.button_FindExit.Size = new System.Drawing.Size(167, 57);
            this.button_FindExit.TabIndex = 0;
            this.button_FindExit.Text = "Найти выход из лабиринта";
            this.button_FindExit.UseVisualStyleBackColor = true;
            this.button_FindExit.Click += new System.EventHandler(this.button_FindExit_Click);
            // 
            // button_SaveMaze
            // 
            this.button_SaveMaze.Location = new System.Drawing.Point(425, 27);
            this.button_SaveMaze.Name = "button_SaveMaze";
            this.button_SaveMaze.Size = new System.Drawing.Size(167, 57);
            this.button_SaveMaze.TabIndex = 1;
            this.button_SaveMaze.Text = "Сохранить картинку";
            this.button_SaveMaze.UseVisualStyleBackColor = true;
            this.button_SaveMaze.Click += new System.EventHandler(this.button_SaveMaze_Click);
            // 
            // pictureMaze
            // 
            this.pictureMaze.Location = new System.Drawing.Point(13, 107);
            this.pictureMaze.Margin = new System.Windows.Forms.Padding(4);
            this.pictureMaze.MinimumSize = new System.Drawing.Size(420, 260);
            this.pictureMaze.Name = "pictureMaze";
            this.pictureMaze.Size = new System.Drawing.Size(579, 419);
            this.pictureMaze.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureMaze.TabIndex = 5;
            this.pictureMaze.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(667, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ширина ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(766, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Высота";
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(769, 48);
            this.txtHeight.Margin = new System.Windows.Forms.Padding(4);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(88, 22);
            this.txtHeight.TabIndex = 8;
            this.txtHeight.Text = "10";
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(644, 48);
            this.txtWidth.Margin = new System.Windows.Forms.Padding(4);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(96, 22);
            this.txtWidth.TabIndex = 9;
            this.txtWidth.Text = "10";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(972, 646);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureMaze);
            this.Controls.Add(this.button_SaveMaze);
            this.Controls.Add(this.button_FindExit);
            this.Controls.Add(this.button_GenMaze);
            this.Name = "Form1";
            this.Text = "Лабиринт";
            ((System.ComponentModel.ISupportInitialize)(this.pictureMaze)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_GenMaze;
        private System.Windows.Forms.Button button_FindExit;
        private System.Windows.Forms.Button button_SaveMaze;
        private System.Windows.Forms.PictureBox pictureMaze;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.TextBox txtWidth;
    }
}

